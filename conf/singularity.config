def checkProfileSingularity(path){
  if (new File(path).exists()){
    File directory = new File(path)
    def contents = []
    directory.eachFileRecurse (groovy.io.FileType.FILES) { file -> contents << file }
    if (!path?.trim() || contents == null || contents.size() == 0){
      println "### ERROR ### The option '-profile singularity' requires the singularity images to be installed on your system. See --singularityImagePath for advanced usage."
      System.exit(-1)
    }
  }else{
    println "### ERROR ### Singularity image path [${path}] not found with. See --singularityImagePath for advanced usage."
    System.exit(-1)
  }
}

singularity {
  enabled = true
  autoMounts = true
  runOptions = "${params.geniac.containers.singularityRunOptions}"
}

process {
  checkProfileSingularity("${params.geniac.singularityImagePath}")
  withLabel:dataset { container = "${params.geniac.singularityImagePath}/dataset.simg" }
  withLabel:ccs { container = "${params.geniac.singularityImagePath}/ccs.simg" }
  withLabel:lima { container = "${params.geniac.singularityImagePath}/lima.simg" }
  withLabel:isoseq3 { container = "${params.geniac.singularityImagePath}/isoseq3.simg" }
  withLabel:mapping { container = "${params.geniac.singularityImagePath}/mapping.simg" }
  withLabel:sqanti { container = "${params.geniac.singularityImagePath}/sqanti.simg" }
  withLabel:multiqc { container = "${params.geniac.singularityImagePath}/multiqc.simg" }
  withLabel:python { container = "${params.geniac.singularityImagePath}/python.simg" }
  withLabel:sqanti3 { container = "${params.geniac.singularityImagePath}/sqanti3.simg" }
}

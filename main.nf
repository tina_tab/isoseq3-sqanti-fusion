#!/usr/bin/env nextflow


/*
========================================================================================
                         PacBioIsoSeq
========================================================================================
*/


def helpMessage() {
  if ("${workflow.manifest.version}" =~ /dev/ ){
    devMess = file("$baseDir/assets/devMessage.txt")
    log.info devMess.text
  }

  log.info"""
    pacbioisoseq3 v${workflow.manifest.version}
    ==========================================================

    Usage:
    nextflow run main.nf --samplePlan sample_plan -profile conda
    nextflow run main.nf --samplePlan 'sample_plan.csv' -profile conda --genomeAnnotationPath '/data/annotations/pipelines' --genome 'hg19'

    Mandatory arguments:
      --samplePlan 'SAMPLEPLAN'     Path to sample plan input file (cannot be used with --reads)
      --genome [str]                Name of genome reference. See the `--genomeAnnotationPath` to defined the annotations path
      --genomeAnnotationPath [dir]  Path to genome annotation folder
      -profile PROFILE              Configuration profile to use. test / conda / singularity / cluster (see below)


    Circular Consensus Sequence (CCS):
      --nbChunks [int]             Parallelize CCS via chunk.

    Primer removal:
      --barcode  'BARCODESET'       Path to fasta or barcodeset file

    Other options:
      --outDir 'PATH'               The output directory where the results will be saved
      -name 'NAME'                  Name for the pipeline run. If not specified, Nextflow will automatically generate a random mnemonic
      --metadata 'FILE'             Add metadata file for multiQC report

    Skip options:
      --skip_OICCS                  Disable automatic HiFi reads generation with Sequel IIe
      --skip_mapping                Disable isoforms mapping
      --skip_sqanti                 Disable sqanti step
      --skip_fusion                 Disable find fusion
      --skip_multiqc                Disable MultiQC step

    =======================================================
    Available Profiles

      -profile test                Set up the test dataset
      -profile conda               Build a new conda environment before running the pipeline
      -profile multiconda          Build a new conda environment per process before running the pipeline. Use `--condaCacheDir` to define the conda cache path
      -profile condaPath           Use a pre-build conda environment already installed on our cluster
      -profile singularity         Use the Singularity images for each process
      -profile cluster             Run the workflow on the cluster, instead of locally
    """.stripIndent()
}

println "Project : $workflow.workDir"

// Show help emssage
if (params.help){
  helpMessage()
  exit 0
}

// Has the run name been specified by the user?
//  this has the bonus effect of catching both -name and --name
customRunName = params.name
if( !(workflow.runName ==~ /[a-z]+_[a-z]+/) ){
  customRunName = workflow.runName
}

// Stage config files
multiQCConfigCh = Channel.fromPath(params.multiqcConfig)
outputDocsCh = Channel.fromPath("$baseDir/docs/output.md")


/*
================================================================================
                                   CHANNELS
================================================================================
*/


if (params.dataSet && params.samplePlan){
  exit 1, "Input reads must be defined using either '--dataSet' or '--samplePlan' parameter. Please choose one way"
}

// CCS options:
if (params.nbChunks >= 1 && params.skip_OICCS) {
   println params.nbChunks
   nbChunks=Channel.from(1..params.nbChunks )
   chunks=Channel.from(1..params.nbChunks).max()
} else if (params.nbChunks > 50) { 
  exit 1, "params.nbChunks is maximum 50. Please choose smaller chunk."
} else if (params.nbChunks < 0) { 
  exit 1, "params.nbChunks is negative: Please choose between 1 to 50."
}

// Barcodeset - lima
if (params.barcode){
  Channel
    .fromPath(params.barcode, checkIfExists: true)
    .into{barcodeLimaCh; barcodeRefineCh }
} else {
  exit 1, "Please set path to fasta or barcodeset file"
}

// Genome Fasta file
genomeRef = params.genome
params.fasta = genomeRef ? params.genomes[ genomeRef ].fasta ?: false : false
if ( params.fasta ){
  Channel
    .fromPath(params.fasta, checkIfExists: true)
    .into{referenceMappingCh; referenceSqantiCh; referenceFusionCh}
} else {
  exit 1, "Fasta file not found: ${params.fasta}"
}
params.gtf = genomeRef ? params.genomes[ genomeRef ].gtf ?: false : false
if ( params.gtf ){
  Channel
    .fromPath(params.gtf, checkIfExists: true)
    .into{referenceGTFSqantiCh; referenceGTFFusionCh}
} else {
  exit 1, "Gtf file not found: ${params.gtf}"
}

if(params.samplePlan && params.skip_OICCS) {
  Channel
    .from(file("${params.samplePlan}"))
    .splitCsv(header: false)
    .map{row -> [
      row[0],
      file(row[1] + '.subreadset.xml'),
      file(row[1] + '.subreads.bam'),
      file(row[1] + '.subreads.bam.pbi')
    ]}
    .set{callingCCSXMLCh}

  Channel
    .from(file("${params.samplePlan}"))
    .splitCsv(header: false)
    .map{row -> [
      row[0],
      file(row[1] + '.subreadset.xml')
    ]}
    .set{datasetFilesCh}

} else if(params.dataSet && params.skip_OICCS){
  Channel
    .from(params.dataSet)
    .ifEmpty { exit 1, "params.dataSet was empty - no input files supplied" }
    .map{row -> [
      file(params.dataSet).name.toString().tokenize('.').get(0),
      file(params.dataSet + '.subreadset.xml'),
      file(params.dataSet +  '.subreads.bam'),
      file(params.dataSet +  '.subreads.bam.pbi')
    ]}
    .set{callingCCSXMLCh}

  Channel
    .from(params.dataSet)
    .map{row -> [
      file(params.dataSet).name.toString().tokenize('.').get(0),
      file(params.dataSet + '.subreadset.xml'),
      file(params.dataSet + '.subreads.bam'),
      file(params.dataSet + '.subreads.bam.pbi')
    ]}
    .set{datasetFilesCh}
}

/*
=========================
        For Sequel2e
=========================
*/

if(params.samplePlan && !params.skip_OICCS){
  Channel
    .from(file("${params.samplePlan}"))
    .splitCsv(header: false)
    .map{row -> [
      row[0],
      file(row[1] + '.consensusreadset.xml'),
      file(row[1] + '.reads.bam'),
      file(row[1] + '.reads.bam.pbi')
    ]}
    .set{extracthifiCh}

  Channel
    .from(file("${params.samplePlan}"))
    .splitCsv(header: false)
    .map{row -> [
      row[0],
      file(row[1] + '.consensusreadset.xml')
    ]}
    .set{datasetFilesCh}
}


if ( params.metadata ){
  Channel
    .fromPath( params.metadata )
    .ifEmpty { exit 1, "Metadata file not found: ${params.metadata}" }
    .set { metadataCh }
}

// Header log info
if ("${workflow.manifest.version}" =~ /dev/ ){
  devMess = file("$baseDir/assets/devMessage.txt")
  log.info devMess.text
}

/*
================================================================================
                                   SUMMARY
================================================================================
*/


log.info """=======================================================

pbIsoSeq3 v${workflow.manifest.version}"
======================================================="""

summary = [
  'Pipeline Name': 'pbIsoSeq3',
  'Pipeline Version': workflow.manifest.version,
  'Run Name': customRunName ?: workflow.runName,
  'Metadata': params.metadata,
  'SamplePlan': params.samplePlan ?: null,
  'DataSet': !params.samplePlan && params.dataSet ?: null,
  'Max Memory': params.maxMemory,
  'Max CPUs': params.maxCpus,
  'Max Time': params.maxTime,
  'Container Engine': workflow.containerEngine,
  'Current home': "$HOME",
  'Current user': "$USER",
  'Current path': "$PWD",
  'Working dir': workflow.workDir,
  'Output dir': params.outDir,
  'Config Profile': workflow.profile
].findAll{ it.value != null }

log.info summary.collect { k,v -> "${k.padRight(15)}: $v" }.join("\n")
log.info "========================================="

/* Creates a file at the end of workflow execution */
workflow.onComplete {
  File woc = new File("${params.outDir}/workflowOnComplete.txt")
  Map endSummary = [:]
  endSummary['Completed on'] = workflow.complete
  endSummary['Duration']     = workflow.duration
  endSummary['Success']      = workflow.success
  endSummary['exit status']  = workflow.exitStatus
  endSummary['Error report'] = workflow.errorReport ?: '-'
  String endWfSummary = endSummary.collect { k,v -> "${k.padRight(30, '.')}: $v" }.join("\n")
  println endWfSummary
  String execInfo = "Summary\n${endWfSummary}\n"
  woc.write(execInfo)
}


/*
 * STEP 0 - Merge -Subreadset
*/
process mergingSubreadSet {
  label 'dataset'
  label 'lowCpu'
  label 'minMem'

  publishDir "${params.outDir}/mergingSubreadSet", mode: 'copy'

  input:
  tuple val(name), file(subreadset) from datasetFilesCh.groupTuple(by: 0/* by name */).dump(tag: 'mergeSubReads')

  output:
  tuple val(name), file("*_merged_*.xml") into mergedSubreadsetReportCh
  file("*merged_*.xml") into mergingSubreadsetAllCh

  script:
  if (!params.skip_OICCS) {
  """
  ###should be checked
  readlink -f *.consensusreadset.xml > ${name}_merged_consensusreadset.fofn
  dataset create ${name}_merged_dataset.xml ${name}_merged_consensusreadset.fofn --type DataSet --force
  """
  } else {
  """
  readlink -f *.subreadset.xml > ${name}_merged_subreadset.fofn
  dataset create ${name}_merged_subreadset.xml ${name}_merged_subreadset.fofn --type SubreadSet --force
  """
  }
}
 

/*
 * Overview Report : *_overview.csv
*/
process overviewReport {
  label 'python'
  label 'lowCpu'
  label 'minMem'

  publishDir "${params.outDir}/mergingSubreadSet", mode: 'copy'

  input:
  tuple val(name), file(mergedxml) from mergedSubreadsetReportCh

  output:
  file("*overview.csv") into overviewCh

  script:
  """
  overviewReport.py --s ${mergedxml} --name ${name}
  """
}



if(!params.skip_OICCS) {
/*
 * extract HIFI
*/

process extracthifi {
  label 'extracthifi'
  label 'medCpu'
  label 'medMem'
  publishDir "${params.outDir}/CCS", mode: 'copy'

  input:
  tuple val(name), file(consensusreadset), file(reads), file(reads_pbi) from extracthifiCh

  output:
  tuple val(name),file("*.ccs.{bam,bai}") into input_hifiReportCh, input_LimaCh

  script:
//  prefix = reads.toString() - ~/(\.reads.bam)/
  """
  extracthifi ${reads} ${name}.hifi.bam -j ${task.cpus} 
  samtools sort ${name}.hifi.bam --threads 4 > ${name}.ccs.bam
  samtools index ${name}.ccs.bam
  """
}


} else { 
/*
 * Generate CCS
*/

  callingCCSCh = callingCCSXMLCh.combine(nbChunks).dump(tag:'chunks')

  process callingCCS {
    label 'ccs'
    label 'medCpu'
    label 'highMem'
    publishDir "${params.outDir}/CCS", mode: 'copy'

    input:
    tuple val(name), file(subreadset), file(subreads), file(subreads_pbi), val(nbChunks) from callingCCSCh
    val(chunks)

    output:
    tuple val(name), file("*.ccs.bam") into chunksCCSBamsCh
    tuple val(name), file("*consensusreadset.xml") into chunksCCSConsensusReadSetCh
    tuple val(name), file("*ccs.report.txt") into reportCh

    script:
    prefix = subreadset.toString() - ~/(\.subreadset.xml)/
    if (params.nbChunks > 1) {
    """
    echo $name
    ccs ${subreadset} ${prefix}.${nbChunks}.ccs.consensusreadset.xml \
     --chunk ${nbChunks}/${chunks}  \
     --max-length 15000 --min-length 50 \
     --min-passes 3 --min-rq 0.9 \
     --log-level INFO -j ${task.cpus} \
     --report-file ${prefix}.ccs.report.txt \
     --log-file ${prefix}.ccs.log

     """
     } else {
     """
     ccs ${subreadset} ${prefix}.ccs.consensusreadset.xml \
      --max-length 15000 --min-length 50 \
      --min-passes 3 --min-rq 0.9 \
      --log-level INFO -j ${task.cpus} \
      --report-file ${prefix}.ccs.report.txt
     """
     }
  }

  process mergingCCS {
    label 'ccs'
    label 'minCpu'
    label 'minMem'
    publishDir "${params.outDir}/mergingCCS", mode: 'copy'

    input:
    tuple val(name), file(bams) from chunksCCSBamsCh.groupTuple(by: 0/* by name */).dump(tag: 'Bams')
    tuple val(name), file(consensusreadset) from chunksCCSConsensusReadSetCh.groupTuple(by: 0/* by name */).dump(tag: 'Bams')

    output:
    tuple val(name),file("*.ccs.{bam,pbi}") into input_hifiReportCh, input_LimaCh, mergingCCSCh
    tuple val(name),file("*.consensusreadset.xml") into mergingCCSConsensusReadSetCh

    script:
    """
    readlink -f *.ccs.bam > ${name}.bamlist.fofn
    pbmerge -o ${name}.ccs.bam ${name}.bamlist.fofn
    readlink -f *.consensusreadset.xml > ${name}.xmllist.fofn
    dataset create ${name}.consensusreadset.xml ${name}.xmllist.fofn --type ConsensusReadSet --force
    """
  }
}


/*
 * STEP 1.b - CCS Report : ccs_npasses_hist.png, ccs_readlength_hist.png, ccs.report.csv, ccs.report.json, By Movie
*/
process ccsReport {
  label 'python'
  label 'lowCpu'
  label 'medMem'

  publishDir "${params.outDir}/ccsReport", mode: 'copy'

  input:
  tuple val(name), file(ccs) from input_hifiReportCh

  output:
  file("*.{csv,png}") into ccsReportCh

  script:
  """
  ccsReport.py --ccs ${name}.ccs.bam --n ${name}
  """
}


/*
 * STEP 2 - Primer removal and demultiplexing
*/
process removePrimers {
  label 'lima'
  label 'medCpu'
  label 'minMem'

  publishDir "${params.outDir}/lima", mode: 'copy'

  input:
  tuple val(name), file(ccs) from input_LimaCh
  file(barcode) from barcodeLimaCh.collect()

  output:
  tuple val(name), file("*.fl.*") into limaBamsCh, limaCh
  tuple val(name), file("*.fl.*.{bam,pbi}") into limaRefineCh
  tuple val(name), file("*.fl.datastore.consensusreadset.xml") into limaRefineChxml

  script:
  prefix = ccs[0].toString() - ~/(\.ccs.bam)/
  """
  lima --isoseq \
       ${prefix}.ccs.bam \
       $barcode \
       ${prefix}.fl.datastore.json \
       --peek-guess \
       --log-level INFO \
       -j ${task.cpus}
  """
}


/*
 * STEP 3 - Trimming of polyA tail
*/
process refine {
  label 'isoseq3'
  label 'lowCpu'
  label 'minMem'

  publishDir "${params.outDir}/refine", mode: 'copy'

  input:
  tuple val(name), file(fl), file(xml) from limaRefineCh.join(limaRefineChxml)
  file(barcode) from barcodeRefineCh.collect()

  output:
  tuple val(name),file("*.flnc.*") into flncCh
  tuple val(name),file("*flnc.consensusreadset.xml") into flncClusteringCh, flncCollapseCh

  script:
  prefix = fl[0].toString() - ~/(\.fl.*.bam)/

  """
  isoseq3 refine \
      ${xml} \
      ${barcode} \
      ${prefix}.flnc.consensusreadset.xml \
      ${prefix}.flnc.filter_summary.json \
      ${prefix}.flnc.report.csv \
      --require-polya \
      --log-level INFO  \
      -j ${task.cpus}
  """
}


/*
 * Barcode Isoseq3 Report : barcode_isoseq3.report.json, Primer Read Binned Histograms(binned_bcqual.json.gz & binned_readlength.json.gz), Primer Quality Score Distribution(bq_histogram.png), fulllength_nonconcatemer_readlength_hist.png, nreads_histogram.png, Number Od reads per primer (nreads.png)
*/

process barcodeIsoseqReport {
  label 'python'
  label 'lowCpu'
  label 'medMem'

  publishDir "${params.outDir}/barcodeIsoseqReport", mode: 'copy'

  input:
  tuple val(name), file(fl), file(flnc) from limaCh.join(flncCh)

  output:
  file("*.{csv,png}") into barcodeIsoseq3ReportCh

  script:
  prefix = fl[0].toString() - ~/(\.fl.datastore.*.bam)/
  bamfile=fl[0].toString() - ~ /(\.bam)/
  """
  barcodeIsoseq3Reports.py \
      --a ${prefix}.fl.datastore.lima.report \
      --p ${bamfile}.bam \
      --c ${prefix}.fl.datastore.lima.counts \
      --f ${prefix}.flnc.report.csv \
      --s ${prefix}.flnc.filter_summary.json \
      --n ${prefix}
  """
}

/*
 * STEP 4 - Clustering
*/
process clustering {
  label 'isoseq3'
  label 'medCpu'
  label 'extraMem'

  publishDir "${params.outDir}/clustering", mode: 'copy'

  input:
//  file('refine/*') from flncClusteringCh.collect()

  tuple val(name), file(flnc) from flncClusteringCh

  output:
  tuple val(name), file("*.{bam,pbi}") into clusteredReportCh
  tuple val(name), file("*.xml") into clusteredMappingCh
  tuple val(name), file("*.fasta.gz") into clusteredfastaCh

  script:
  prefix = flnc[0].toString() - ~/(\.flnc.*.xml)/

  """
  ##readlink -f refine/*.flnc.consensusreadset.xml > flnc.fofn
  isoseq3 cluster ${prefix}.flnc.consensusreadset.xml ${prefix}.clustered.bam  --use-qvs --log-level INFO -j ${task.cpus}
  """
}


/*
 * STEP 5 - Mapping
*/
process mapping {
  label 'mapping'
  label 'medCpu'
  label 'highMem'

  publishDir "${params.outDir}/mapping", mode: 'copy'

  when:
  !params.skip_mapping

  input:
  tuple val(name), file(clustered) from clusteredMappingCh
  file(reference) from referenceMappingCh.collect()

  output:
  tuple val(name), file("*.{bam,pbi,bai}") into mappingCollapseCh, mappingRepCh, mappingFusionCh

  script:
  prefix = clustered[0].toString() - ~/(\.clustered.transcriptset.xml)/

  """
  pbmm2 align --sort\
  ${reference} \
  ${prefix}.clustered.transcriptset.xml \
  ${prefix}.mapped.transcriptalignmentset.xml \
  --min-concordance-perc 95.0 \
  --min-length 50 \
  --bam-index BAI \
  --preset ISOSEQ \
  --log-level DEBUG \
  -j ${task.cpus}
  """
}


// data/kdi_prod/project_result/789/07.00/hg38_base/referenceset.xml

/*
 * STEP 7 - Collapse
*/
process collapse {
  label 'isoseq3'
  label 'lowCpu'
  label 'medMem'

  publishDir "${params.outDir}/collapse", mode: 'copy'

  when:
  !params.skip_mapping

  input:
  tuple val(name), file(mapped), file(flnc) from mappingCollapseCh.join(flncCollapseCh)
  
  output:
  tuple val(name), file("*.collapse.gff") into collapseCh
  tuple val(name), file("*.report.json") into collapseReportCh
  tuple val(name), file("*.collapse.fasta") into collapsefastaCh


  script:
  prefix = mapped[0].toString() - ~/(\.mapped.bam)/
  """
  isoseq3 collapse ${prefix}.mapped.bam ${prefix}.flnc.consensusreadset.xml ${prefix}.collapse.fastq -j ${task.cpus}
  """
}

/*
 * Isoseq3 Report :  hq_lq_isoforms_avgqv_hist.png, consensus_isoforms_readlength_hist.png
*/
process clustering_mapping_isocolaps {
  label 'python'
  label 'lowCpu'
  label 'minMem'

  publishDir "${params.outDir}/clustering_mapping_isocolaps", mode: 'copy'

  when:
  !params.skip_mapping

  input:
  tuple val(name), file(clustered), file (mapped), file (collapse_report) from clusteredReportCh.join(mappingRepCh).join(collapseReportCh)
  
  output:
  file("*.{csv,png}") into isoseq3ReportCh

  script:
  prefix = clustered[0].toString() - ~/(\.clustered.bam)/

  """
  clustering_mapping_isocolaps.py --a ${prefix}.mapped.bam --r ${prefix}.collapse.report.json --hq ${prefix}.clustered.hq.bam --lq ${prefix}.clustered.lq.bam --n ${prefix}
  rm ${prefix}.clustered.hq.csv ${prefix}.clustered.lq.csv ${prefix}_transcript_mapping_summary_metrics.csv
  """
}


/*
================================================================================
                                   STEP 8 - SQANTI
================================================================================
*/
process sqanti {
  label 'sqanti3'
  label 'medCpu'
  label 'medMem'
  publishDir "${params.outDir}/sqanti", mode: 'copy'

  when:
  !params.skip_sqanti & !params.skip_mapping

  input:
  tuple val(name), file(collapse) from collapseCh
  file(reference) from referenceSqantiCh.collect()
  file(referenceGTFSqanti) from referenceGTFSqantiCh.collect()

  output:
  tuple val(name), file("*_classification.txt") into sqantiClassificationCh
  tuple val(name), file("*_junctions.txt") into sqantiJunctionsCh
  tuple val(name), file("*_SQANTI3_report.{html,pdf}") into sqantiCh
  tuple val(name), file("*.{fasta,gtf}") into sqantimatricsCh
  script:
  prefix = collapse[0].toString() - ~/(\.collapse.gff)/
  """
  gffread ${prefix}.collapse.gff -T -o ${prefix}.collapse.gtf
  sqanti3_qc.py ${prefix}.collapse.gtf $referenceGTFSqanti $reference -t ${task.cpus}
  sqanti3_RulesFilter.py ${prefix}.collapse_classification.txt ${prefix}.collapse_corrected.fasta ${prefix}.collapse.gtf
  """
}

/*
 * SQANTI Report
*/
process sqantiReport {
  label 'lowCpu'
  label 'minMem'
  publishDir "${params.outDir}/sqantiReport", mode: 'copy'

  when:
  !params.skip_sqanti & !params.skip_mapping

  input:
//  tuple val(name), file(classification) from sqantiClassificationCh
  tuple val(name),file(classification), file(junctions) from sqantiClassificationCh.join(sqantiJunctionsCh)

  output:
  file("*.csv") into sqantiReportCh

  script:
  prefix = classification[0].toString() - ~/(\.collapse_classification.filtered_lite_classification.txt)/
  """
  sqantiReport.sh ${prefix}.collapse_classification.filtered_lite_classification.txt ${prefix}.collapse_classification.filtered_lite_junctions.txt ${prefix}
  """
}

/*
================================================================================
                                   STEP 9 - Finding fusion genes
================================================================================
*/
process fusion_finder {
  label 'sqanti3'
  label 'medCpu'
  label 'highMem'

  publishDir "${params.outDir}/fusion_finder", mode: 'copy'

  when:
  !params.skip_fusion

  input:
  tuple val(name), file (mapped) from mappingFusionCh
  file(reference) from referenceFusionCh.collect()
  file(referenceGTF) from referenceGTFFusionCh.collect()

  output:
  tuple val(name), file("*.{fusion.annotated.txt,fusion.annotated_ignored.txt}") into fusionFinderCh
  file("*.csv") into fusionFinderReportCh 
  script:
  prefix = mapped[0].toString() - ~/(\.mapped.bam)/

  """
  samtools fastq ${prefix}.mapped.bam > ${prefix}.fastq
  minimap2 -ax splice -t 4 -uf --secondary=no -C5 ${reference} ${prefix}.fastq > ${prefix}.fastq.sam
  sort -k 3,3 -k 4,4n ${prefix}.fastq.sam > ${prefix}.fastq.sorted.sam
  fusion_finder.py --input ${prefix}.fastq --fq -s ${prefix}.fastq.sorted.sam -o ${prefix}_isoforms.fasta.fusion
  sqanti3_qc.py ${prefix}_isoforms.fasta.fusion.gff ${referenceGTF} ${reference} --is_fusion -d sqanti_for_fusion_${prefix}
  fusion_collate_info.py ${prefix}_isoforms.fasta.fusion  sqanti_for_fusion_${prefix}/${prefix}_isoforms.fasta.fusion_classification.txt ${referenceGTF} --genome ${reference}
  cut -f 1,3,4,5,7,8,9,17,18 -d "," ${prefix}_isoforms.fasta.fusion.annotated.txt|awk 'NR!=1{\$1="${prefix}_"\$1}1'|sed 's/,/\t/g' > ${name}_isoforms.fasta.fusion.annotated.selected.csv
  """
}

/*
================================================================================
                                   MultiQC
================================================================================
*/
process multiqc {
  label 'multiqc'
  label 'lowCpu'
  label 'minMem'

  publishDir "${params.outDir}/MultiQC", mode: 'copy'

  input:
  file(samplePlan) from Channel.of( params.samplePlan ? file("$params.samplePlan") : "")
  file(metadata) from metadataCh.ifEmpty([])
  file(multiqcConfig) from multiQCConfigCh
  file('mergingSubreadset/*') from overviewCh.collect().ifEmpty([])
  file('ccsReport/*') from ccsReportCh.collect().ifEmpty([])
  file('barcodeIsoseqReport/*') from barcodeIsoseq3ReportCh.collect().ifEmpty([])
  file('clustering_mapping_isocolaps/*') from isoseq3ReportCh.collect().ifEmpty([])
  file('sqantiReport/*') from sqantiReportCh.collect().ifEmpty([])
  file('fusion_finder/*') from fusionFinderReportCh.collect().ifEmpty([])

  when:
  !params.skip_multiqc

  output:
  file("*_report.html") into multiqcReportCh
  file("*_data")

  script:
  rtitle = customRunName ? "--title \"$customRunName\"" : ''
  rfilename = customRunName ? "--filename " + customRunName + "_PacBioIsoSeq_report" : '--filename PacBioIsoSeq_report'
  metadata_opts = params.metadata ? "--metadata ${metadata}" : ""
  splan_opts = params.samplePlan ? "--splan ${samplePlan}" : ""
  tmreport = params.skip_mapping ? 0 : 1
  """
  mqcHeader.py --name "PacBioIsoSeq" --version ${workflow.manifest.version} ${metadata_opts} ${splan_opts} > multiqc-config-header.yaml
  createImages.sh ${tmreport}
  multiqc.sh ${rtitle} ${rfilename} multiqc-config-header.yaml ${multiqcConfig} ${tmreport} 
  """
}


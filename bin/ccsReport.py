#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""ccsReport.py: TODO
   python ccsReport.py --b ccs.bam
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pysam
import re
import os
import json
import sys
from optparse import OptionParser


class CCSReport(object):
    """
    TODO
    """

    def get_options(self):
        usage = "usage: %prog [options] arg"
        parser = OptionParser(usage)
        parser.add_option("--ccs", "--ccs", dest="ccs", default="", help="ccs output format bam")
        parser.add_option("--n", "--name", dest="name", default="", help="")

        # TODO: Ellipsis should probably be removed
        ...
        (options, args) = parser.parse_args()
        ...

        args = []
        args = self.check_options(options.ccs, options.name)
        return (args)

    def check_options(self, ccs, name):
        """
          Check arguments in command lign.
        """
        if ccs != "":
            ccs_bams = ccs
        else:
            print('No ccs.bam. Exiting.')
            exit(0)

        if name != "":
            name = name
        else:
            base = os.path.basename(ccs)
            name = os.path.splitext(base)[0].split('.ccs')[0]
        args = []
        args = (ccs_bams, name)
        return (args)

    def pars_ccs(self, bams, bams_header):
        """
           parsing lima_output.Clontech_5p--NEB_Clontech_3p.bam
           awk '{sum+=$1} END {print sum; }' info.csv
         """
        try:
            ccs_rep_list = []
            sum_len = 0
            sum_passes = 0
            total_ccs = 0

            faHandler = open(bams_header, 'w')
            save = pysam.set_verbosity(0)
            samfile = pysam.AlignmentFile(bams, "rb", check_sq=False)
            pysam.set_verbosity(save)
            faHandler.write("Sequence" + "," + "len" + "," + "npass" + "\n")
            for read in samfile.fetch(until_eof=True):
                total_ccs = total_ccs + 1
                sum_len = sum_len + read.query_length
                sum_passes = sum_passes + read.get_tag('np')
                #quality.append(read.qual.count("*"))
                faHandler.write("%s,%s,%s\n" % (read.query_name, read.query_length, read.get_tag('np')))
            mean_len = round(sum_len / total_ccs)
            mean_passes = round(sum_passes / total_ccs)
            faHandler.close()
            #print(np.median(np.array(quality)))
            ccs_rep_list.append(total_ccs)
            ccs_rep_list.append(sum_len)
            ccs_rep_list.append(mean_len)
            ccs_rep_list.append(mean_passes)
            return (ccs_rep_list)
        except ValueError:
            print("Oops! Unexpected error in pars_ccs_bam .  Try again...", sys.exc_info()[0])

    def generate_plots(self, ccs_info, name):

        try:
            sns.set(style="darkgrid")
            df = pd.read_csv(ccs_info, delimiter=",")
            n, bins, patches = plt.hist(df['len'], 100, density=False, facecolor='b', alpha=0.75)
            plt.xlabel(" Read Length (bp)", fontsize=10)
            plt.ylabel("Reads", fontsize=10)
            # plt.xscale('log')
            # plt.ylim([1000,40000])
            plt.grid(False)
            # plt.xlim([500,15000])
            plt.title('CCS Read Length')
            plt.tight_layout()
            plt.savefig(name + '_ccs_readlengths.png')
            plt.close()

            df = pd.read_csv(ccs_info, delimiter=",")
            n, bins, patches = plt.hist(df['npass'], 200, density=False, facecolor='b', alpha=0.75)
            plt.xlabel("Number of Passses", fontsize=10)
            plt.ylabel("Reads", fontsize=10)
            plt.grid(False)
            # plt.xlim([0,100])
            plt.title('Number of Passes')
            plt.tight_layout()
            plt.savefig(name + '_number_of_passes.png')
            plt.close()

        except ValueError:
            print("Oops! Unexpected error in generate_plots.  Try again...", sys.exc_info()[0])

    def write_result(self, dict, output):
        """
        This file is read by MultiQC.
        """
        with open(output, 'w') as out:
            out.write('\t'.join(str(key) for key in dict) + '\n')
            out.write('\t'.join(str(value) for value in dict.values()) + '\n')


if __name__ == '__main__':
    print("start")
    CCSR = CCSReport()
    args = CCSR.get_options()
    ccs_bams = args[0]
    name = args[1]

    ########################
    ###### Preparing tables
    ########################
    pars_ccs = CCSR.pars_ccs(ccs_bams, name + '_ccs_info.csv')

    summary_metrics_dict = {
        "BioSample": name,
        "CCS_Reads": pars_ccs[0],
        "Number_of_CCS_bases": pars_ccs[1],
        "CCS_Read_Length_mean": pars_ccs[2],
        "Number_of_Passes_mean": pars_ccs[3]
    }

    CCSR.write_result(summary_metrics_dict, name + '_ccs_summary_metrics.csv')

    #    with open(name + '_ccs_summary_metrics.csv' , 'w') as out:
    #         out.write("Analysis Metric" + "\t" + "Value" + "\n")
    #         for key, value in summary_metrics_dict.items():
    #             out.write("%s\t%d\n" %(key,value))

    ########################
    ###### Preparing plots
    ########################
    CCSR.generate_plots((name + "_ccs_info.csv"), name)

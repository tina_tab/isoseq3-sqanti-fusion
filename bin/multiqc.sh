#!/bin/bash

rtitle=$1
rfilename=$2
mqc_config_header=$3
multiqc_config=$4
tmreport=$5


## Put plots in alphabetical order to show them in order

string_img=""
for i in $(ls -1 plots/ | grep -v Mapping|sort -t'_' -n -k2); do string_img=$string_img" ""plots/"${i%%/}; done

multiqc */*.csv $string_img -f ${rtitle} ${rfilename} -c ${mqc_config_header} -c ${multiqc_config}


#if [ $tmreport == "1" ]; then
#  multiqc */*.csv plots/Transcript\ Mapping_mqc.png $string_img -f ${rtitle} ${rfilename} -c ${mqc_config_header} -c ${multiqc_config}
#else
#  multiqc */*.csv $string_img -f ${rtitle} ${rfilename} -c ${mqc_config_header} -c ${multiqc_config}
#fi

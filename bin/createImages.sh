#!/bin/bash

tmreport=$1
mkdir plots

ARRAY=(`ls mergingSubreadset/*.csv | awk -F_over '{print $1}' |cut -f 2 -d "/" |sort --unique`)

for i in ${ARRAY[*]}; do
    echo "$i"
    if [ $tmreport == "1" ]; then
       convert ccsReport/${i}_ccs_readlengths.png ccsReport/${i}_number_of_passes.png clustering_mapping_isocolaps/${i}_Read_Length_of_Mapped_Isoforms.png +append plots/${i}_ccs_report.png
    else
       convert ccsReport/${i}_ccs_readlengths.png ccsReport/${i}_number_of_passes.png +append plots/${i}_ccs_report.png
    fi
    convert barcodeIsoseqReport/${i}_flnc_readlengths.png barcodeIsoseqReport/${i}_binned_readlengths.png barcodeIsoseqReport/${i}_binned_readscores.png +append plots/${i}_barcode_isoseq_report.png
    convert plots/${i}_ccs_report.png plots/${i}_barcode_isoseq_report.png -append plots/${i}_mqc.png
    rm plots/${i}_ccs_report.png plots/${i}_barcode_isoseq_report.png
done

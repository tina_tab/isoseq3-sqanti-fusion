#!/usr/bin/env python

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pysam
#from sklearn.cluster import KMeans
import re
import os
import json
import sys
from optparse import OptionParser

class clustering_mapping_isocolaps(object):
    """
       python isoseq3TranscriptAlignReport.py --a mapped.hq.bam --r out.report.json
    """
    def get_options(self):
        usage = "usage: %prog [options] arg"
        parser = OptionParser(usage)
        parser.add_option("--a", "--align_isoforms", dest="align_isoforms", default="", help="align isoforms format bam")
        parser.add_option("--r", "--report", dest="report", default="", help="isocollapse report format json")
        parser.add_option("--hq", "--hq_isoforms", dest="hq_isoforms", default="", help="high-quality isoforms format bam")
        parser.add_option("--lq", "--lq_isoforms", dest="lq_isoforms", default="", help="low-quality isoforms format bam")
        parser.add_option("--n", "--name", dest="name", default="", help="")
        ...
        (options, args) = parser.parse_args()
        ...
        args = []
        args = self.check_options(options.align_isoforms, options.report, options.hq_isoforms, options.lq_isoforms, options.name)
        ##print(args)
        return(args)

    def check_options(self, align_isoforms, report, hq_isoforms, lq_isoforms, name):
        """
          Check arguments in command lign.
        """
        if align_isoforms != "":
           align_isoforms = align_isoforms
        else:
           print ('No isoforms. Exiting.')
           exit(0)         

        if report != "":
           report = report
        else:
           print ('No collapse report. Exiting.')
           exit(0)

        if hq_isoforms != "":
           hq_isoforms = hq_isoforms
        else:
           print ('No high quality isoforms. Exiting.')
           exit(0)

        if lq_isoforms!= "":
           lq_isoforms = lq_isoforms
        else:
           print ('No low quality isoforms. Exiting.')
           exit(0)

        if name != "":
           name = name
        else:
           base = os.path.basename(align_isoforms)
           name= os.path.splitext(base)[0].split('.clustered')[0]
       
        args = []
        args = (align_isoforms, report, hq_isoforms, lq_isoforms, name)
        return(args)

    def pars_mapped_trx(self, isoforms, info):
         """
           parsing bam files
         """
         try:
           faHandler = open(info, 'w')
           save=pysam.set_verbosity(0)
           samfile = pysam.AlignmentFile(isoforms, "rb", check_sq=False)
           pysam.set_verbosity(save)
           faHandler.write("transcript" + "\t" + "len" +  "\n")
           for read in samfile.fetch(until_eof=True):
               faHandler.write("%s\t%s\n" %(read.query_name,read.query_length))
           faHandler.close()
         except ValueError:
           print("Oops! Unexpected error in parse_isoforms .  Try again...", sys.exc_info()[0])


    def parse_report(self, report, name):
        """
          parsing report od isocollaps "out.report.json"
        """
       # isocollapse=[]
       # try:
       #    with open(report) as json_file:
       #       data = json.load(json_file)
       #       isocollapse.append(name)
       #       for i, item in enumerate(data['attributes']):
       #            if 'isoseq_mapping.number_mapped_unique_isoforms' in item.get("id"):
       #                isocollapse.append(item.get("value"))
       #            if 'isoseq_mapping.number_of_mapped_unique_loci' in item.get("id"):
       #                isocollapse.append(item.get("value"))
       #    return(isocollapse)
       # except ValueError:
       #    print("Oops! Unexpected error in pars_report .  Try again...", sys.exc_info()[0])

        isocollapse=[]
        try:
           with open(report) as json_file:
              data = json.load(json_file)
              isocollapse.append(name)
              for i, item in enumerate(data['attributes']):
                   if 'number_mapped_unique_isoforms' == item.get("id"):
                       isocollapse.append(item.get("value"))
                   if 'number_of_mapped_unique_loci' == item.get("id"):
                       isocollapse.append(item.get("value"))
           print(isocollapse)
           return(isocollapse)
        except ValueError:
           print("Oops! Unexpected error in pars_report .  Try again...", sys.exc_info()[0])



    def collect_primer_reads(self, primers, primers_header):
         """
           parsing lima_output.Clontech_5p--NEB_Clontech_3p.bam
         """
         try:
           faHandler = open(primers_header, 'w')
           save=pysam.set_verbosity(0)
           samfile = pysam.AlignmentFile(primers, "rb", check_sq=False)
           pysam.set_verbosity(save)
           faHandler.write("Sequence" +"\n")
           for read in samfile.fetch(until_eof=True):
               faHandler.write(read.query_name.replace("/ccs","") +"\n")
           faHandler.close()
         except ValueError:
           print("Oops! Unexpected error in collect_primer_reads .  Try again...",sys.exc_info()[0])


    def pars_isoforms(self, isoforms, info):
         """
           parsing polished.hq.bam or polished.lq.bam
         """
         try:
           iso_rep_list = []
           total_iso = 0
           faHandler = open(info, 'w')
           save=pysam.set_verbosity(0)
           samfile = pysam.AlignmentFile(isoforms, "rb", check_sq=False)
           pysam.set_verbosity(save)
           faHandler.write("transcript" + "\t" + "len" + "\t" + "subreads" + "\n")
           for read in samfile.fetch(until_eof=True):
               total_iso = total_iso + 1
               faHandler.write("%s\t%s\t%s\n" %(read.query_name,read.query_length,read.get_tag('im')))
           faHandler.close()
           iso_rep_list.append(total_iso)
           return(iso_rep_list)
         except ValueError:
           print("Oops! Unexpected error in isoforms .  Try again...",sys.exc_info()[0])

    def generate_plots(self, info, filename, title):
      try:
          sns.set(style="darkgrid")
          df=pd.read_csv(info, delimiter="\t")
          n, bins, patches = plt.hist(df['len'], 100,  density=False, facecolor='b', alpha=0.75)
          plt.xlabel(" Read Length (bp)", fontsize=10)
          plt.ylabel("Reads",fontsize=10)
          #plt.xscale('log')
          #plt.ylim([1000,40000])
          plt.grid(False)
          #plt.xlim([500,15000])
          plt.title(title)
          plt.tight_layout()
          plt.savefig(filename)
          plt.close()

      except ValueError:
           print("Oops! Unexpected error in generate_plots.  Try again...",sys.exc_info()[0])

    def write_result(self, dict, output):
        """
        This file is read by MultiQC.
        """
        with open(output, 'w') as out:
            out.write('\t'.join(str(key) for key in dict) + '\n')
            out.write('\t'.join(str(value) for value in dict.values()) + '\n')


if __name__ == '__main__':
    CMI = clustering_mapping_isocolaps()
    args = CMI.get_options()
    align_isoforms =  args[0]
    report = args[1]
    hq_isoforms =  args[2]
    lq_isoforms =  args[3]
    name = args[4]
   
    ########################
    ###### Preparing plots
    ########################

    outputFile = name + '_transcript_mapping_summary_metrics.csv'
    CMI.pars_mapped_trx(align_isoforms, outputFile)
    file_png = name + '_Read_Length_of_Mapped_Isoforms.png'
    CMI.generate_plots(outputFile, file_png, "Read Length of Mapped Isoforms")
   
    ########################
    ###### pre_preparing tables isoforms
    ########################
    hq_info = os.path.splitext(os.path.basename(hq_isoforms))[0]+ '.csv'
    hq_iso_rep_list=CMI.pars_isoforms(hq_isoforms, hq_info)
    lq_info = os.path.splitext(os.path.basename(lq_isoforms))[0]+ '.csv'
    lq_iso_rep_list=CMI.pars_isoforms(lq_isoforms, lq_info)

    isocollapse = CMI.parse_report(report, name)

    summary_metrics_dict = { "BioSample" : isocollapse[0],
    "Number_of_HQ_isoforms" : hq_iso_rep_list[0],
    "Number_of_LQ_isoforms" : lq_iso_rep_list[0],
    "Uniquely_mapped_isoforms" : isocollapse[1],
    "Percent_uniquely_mapped_isoforms" : round(isocollapse[1]*100/(int(lq_iso_rep_list[0]) + int(hq_iso_rep_list[0]))),
    "Uniquely_mapped_loci" : isocollapse[2]
    }
   
    ########################
    ###### Preparing table
    ########################
    CMI.write_result(summary_metrics_dict, name + "_clustring_mapping_isocollapse_metrics.csv")
    


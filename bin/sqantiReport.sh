#!/bin/bash


CLASSIFICATION_FILE=$1
JUNCTIONS_FILE=$2
NAME=$3

############################
##   create category_isoforms.csv
############################
ARRAY=(novel_in_catalog full-splice_match novel_not_in_catalog incomplete-splice_match genic_intron genic intergenic antisense fusion)
echo -e "biosample\tfull-splice_match\tincomplete-splice_match\tnovel_in_catalog\tnovel_not_in_catalog\tgenic_intron\tgenic_genomic\tintergenic\tantisense\tfusion" >  ${NAME}.category_isoforms.csv

printf '%s\t' ${NAME} >> ${NAME}.category_isoforms.csv
ARRAY=(novel_in_catalog full-splice_match novel_not_in_catalog incomplete-splice_match genic_intron genic intergenic antisense fusion)
ARRAY=(full-splice_match incomplete-splice_match novel_in_catalog novel_not_in_catalog genic_intron genic intergenic antisense fusion)
for i in ${ARRAY[*]}; do
    #echo "$i"
    itm=$(grep -c -w "$i" ${CLASSIFICATION_FILE})
    printf '%s\t' ${itm} >> ${NAME}.category_isoforms.csv
done
sed -i 's/[[:blank:]]*$//' ${NAME}.category_isoforms.csv

############################
##   create category_genes.csv
############################
echo -e "biosample\tisoforms\tannotated_genes\tnovel_genes\tgenes\tmean_iso_gene" > ${NAME}.category_genes.csv
ISOFORMS=$(tail -n +2 ${CLASSIFICATION_FILE} | wc -l)
ANNOTATED_GENES=$(tail -n +2 ${CLASSIFICATION_FILE}|cut -f 7 |grep -v 'novelGene_'|sort|uniq|wc -l)
NOVEL_GENES=$(cut -f 7 ${CLASSIFICATION_FILE}|grep 'novelGene_'|sort|uniq|wc -l)
GENES=$((${ANNOTATED_GENES}+${NOVEL_GENES}))
#GENES_PERCENTAGE=$(bc <<< "scale=2;$GENES*100/$ISOFORMS")
Mean_ISO_GENES=$(bc <<< "scale=2;$ISOFORMS/$GENES")
printf '%s\t%s\t%s\t%s\t%s\t%s' ${NAME} ${ISOFORMS} ${ANNOTATED_GENES} ${NOVEL_GENES} ${GENES} ${Mean_ISO_GENES} >> ${NAME}.category_genes.csv

############################
##    create category SJ
############################
echo -e "biosample\tknown_cano\tknown_non_cano\tnovel_cano\tnovel_non_cano" > ${NAME}.category_junctions.csv
KNOWN_CANO="$(awk '{printf("%s_%s_%s\t%s\t%s\t%s\n",$2,$5,$6,$8,$15,$14)}' ${JUNCTIONS_FILE}|grep "known"|grep -v "non_canonical"|sort|uniq|wc -l)"
KNOWN_NON_CANO="$(awk '{printf("%s_%s_%s\t%s\t%s\t%s\n",$2,$5,$6,$8,$15,$14) }' ${JUNCTIONS_FILE}|grep "known"|grep "non_canonical"|sort|uniq|wc -l)"
NOVEL_CANO="$(awk '{printf("%s_%s_%s\t%s\t%s\t%s\n",$2,$5,$6,$8,$15,$14) }' ${JUNCTIONS_FILE}|grep "novel"|grep -v "non_canonical"|sort|uniq|wc -l)"
NOVEL_NON_CANO="$(awk '{printf("%s_%s_%s\t%s\t%s\t%s\n",$2,$5,$6,$8,$15,$14) }' ${JUNCTIONS_FILE}|grep "novel"|grep "non_canonical"|sort|uniq|wc -l)"

#TOTAL_JUN=$((${KNOWN_CANO}+${KNOWN_NON_CANO}+${NOVEL_CANO}+${NOVEL_NON_CANO}))

printf '%s\t%s\t%s\t%s\t%s' ${NAME} ${KNOWN_CANO} ${KNOWN_NON_CANO} ${NOVEL_CANO} ${NOVEL_NON_CANO}  >> ${NAME}.category_junctions.csv

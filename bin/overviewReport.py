#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""overviewReport.py: TODO
   python3.6 overviewReport.py --s m54063_181212_112538.subreadset.xml --name m54063_181212_112538
"""

# TODO: optparse is now deprecated since 3.2, use argparse instead
import os
import xml.etree.ElementTree as ET
from optparse import OptionParser


class OverviewReport(object):
    """
    TODO
    """

    def get_options(self):
        usage = "usage: %prog [options] arg"
        parser = OptionParser(usage)
        parser.add_option("--s", "--subreads", dest="subreads", default="", help="subreadset format xml")
        parser.add_option("--n", "--name", dest="name", default="", help="")

        # TODO: Ellipsis should probably be removed
        ...
        (options, args) = parser.parse_args()
        ...
        args = []
        args = self.check_options(options.subreads, options.name)
        return (args)

    def check_options(self, subreads, name):
        """
          Check arguments in command line.
        """
        if subreads != "":
            subreads = subreads
        else:
            print('No subraeds. Exiting.')
            exit(0)

        if name != "":
            name = name
        else:
            base = os.path.basename(subreads)
            name = os.path.splitext(base)[0]
            print(name)
        args = []
        args = (subreads, name)
        return (args)

    def parse_subreadset_file(self, subreads, name):
        """
          parsing *.subreadset.xml to extract basic informatin.
        """
        try:
            biosample = []
            WellName = []
            cellname = []
            metadata_context_id = []
            instrument_name = []
            kit_version = []
            smrt_link_version = []
            wellcell_name = []
            wellcell_des = []
            createdby = []
            movie_time = []
            whencreated = []
            run_stampname = []
            run_name = []
            wellname = []

            tree = ET.parse(subreads)
            root = tree.getroot()
            cellname = root.attrib['Name']

            metadata = root.find("{http://pacificbiosciences.com/PacBioDatasets.xsd}DataSetMetadata")
            for item in metadata.iter():
                # print(item.tag, "\n", item.attrib, "\n", item.text)
                if item.tag == "{http://pacificbiosciences.com/PacBioCollectionMetadata.xsd}RunDetails":
                    run_stampname.append(item[0].text)
                    run_name.append(item[1].text)
                    createdby.append(item[2].text)
                    whencreated.append(item[3].text.split('T')[0])
                    # [print(elem.text) for elem in item.iter()]

                if item.tag == "{http://pacificbiosciences.com/PacBioCollectionMetadata.xsd}WellName":
                    wellname.append(item.text)

                if item.tag == "{http://pacificbiosciences.com/PacBioSampleInfo.xsd}BioSample":
                    biosample.append(item.attrib.get("Name"))

                ### For each dataset ####
                if item.attrib != {}:
                    # print(item.tag,item.attrib,item.text)

                    if item.tag == "{http://pacificbiosciences.com/PacBioCollectionMetadata.xsd}CollectionMetadata":
                        metadata_context_id.append(item.attrib.get("Context"))
                        instrument_name.append(item.attrib.get("InstrumentName"))

                    if item.tag == "{http://pacificbiosciences.com/PacBioCollectionMetadata.xsd}ControlKit":
                        kit_version.append(item.attrib.get("Version"))
                    # if item.tag == "{http://pacificbiosciences.com/PacBioCollectionMetadata.xsd}CreatedBy":

                    if item.tag == "{http://pacificbiosciences.com/PacBioCollectionMetadata.xsd}WellSample":
                        wellcell_name.append(item.attrib.get("Name"))
                        wellcell_des.append(item.attrib.get("Description"))

                    if item.tag == "{http://pacificbiosciences.com/PacBioBaseDataModel.xsd}AutomationParameter":
                        if item.attrib.get("Name") == "MovieLength":
                            movie_time.append(int(item.attrib.get("SimpleValue")) / 60)

                    if item.tag == "{http://pacificbiosciences.com/PacBioCollectionMetadata.xsd}VersionInfo":
                        if item.attrib.get("Name") == "smrtlink":
                            smrt_link_version.append(".".join(item.attrib.get("Version").split('.')[0:2])
                                                     )
            subreads_dict = dict(
                # BioSample = biosample, ## Attention: Sequel2 has not biosample.
                BioSample=wellcell_name,  # should ask NGS. dont use wellname in dataset name.
                Wellcell_Name=wellname,
                Metadata_Context_Id=metadata_context_id,
                Run_Name=run_name,
                Run_Id=run_stampname,
                Organism=wellcell_des,
                SMRT_Link_Version=smrt_link_version,
                CreatedBy=createdby,
                WhenCreated=whencreated,
                Instrument_Name=instrument_name,
                Kit_Version=kit_version,
                Movie_Time=movie_time
            )
            # print(subreads_dict)
            return (subreads_dict)

        except ValueError:
            return False

    def write_result(self, dict, output):
        """
       This file is read by MultiQC.
       """
        tmp = []
        mylist = []
        with open(output, 'w') as out:
            for key, value in dict.items():
                mylist.append(dict[key])
            out.write('\t'.join(str(key) for key in dict) + '\n')
            for i in range(0, len(mylist[0])):
                for j in range(0, len(mylist)):
                    tmp.append(mylist[j][i])
                out.write('\t'.join(str(value).replace(" ", "") for value in tmp) + '\n')
                tmp = []


if __name__ == '__main__':
    OR = OverviewReport()
    args = OR.get_options()
    subreads = args[0]
    name = args[1]
    subreads_dict = OR.parse_subreadset_file(subreads, name)
    OR.write_result(subreads_dict, name + "_overview.csv")

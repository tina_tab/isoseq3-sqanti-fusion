#!/usr/bin/env python

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pysam
#from sklearn.cluster import KMeans
import re
import os
import json
import sys
from optparse import OptionParser

class barcode_isoseq3_reports(object):
    """
    python barcodeIsoseq3Reports.py --a lima_output.lima.report --p lima_output.Clontech_5p--NEB_Clontech_3p.bam --c lima_output.lima.counts --f flnc.report.csv --s flnc.filter_summary.json
    """
    def get_options(self):
        usage = "usage: %prog [options] arg"
        parser = OptionParser(usage)
        parser.add_option("--a", "--lima_report", dest="all_reads", default="", help="fl.datastore.lima.report format csv")
        parser.add_option("--p", "--lima_output", dest="primers", default="", help="fl.datastore.Clontech_5p--NEB_Clontech_3p.bam format bam")
        parser.add_option("--c", "--lima_count", dest="primer_info", default="", help="fl.datastore.lima.count format csv") 
        parser.add_option("--f", "--flnc_report", dest="flnc_rep", default="", help="flnc.report format csv")
        parser.add_option("--s", "--flnc_filter_summary", dest="flnc_sum", default="", help="flnc_filter_summary format json")
        parser.add_option("--n", "--name", dest="name", default="", help="")
        ...
        (options, args) = parser.parse_args()
        ...

        args = []
        args = self.check_options(options.all_reads, options.primers,options.primer_info, options.flnc_rep, options.flnc_sum, options.name)
        ##print(args)
        return(args)

    def check_options(self, all_reads, primers, primer_info, flnc_rep, flnc_sum, name):
        """
          Check arguments in command lign.
        """
        if all_reads != "":
           all_reads = all_reads
        else:
           print ('No lima reports file. Exiting.')
           exit(0)

        if primers != "":
           primers = primers
        else:
           print ('No primers. Exiting.')
           exit(0)

        if name != "":
           name = name
        else:
           base = os.path.basename(all_reads)
           name= os.path.splitext(base)[0].split('.fl')[0]
        args = []
        args = (all_reads, primers, primer_info, flnc_rep, flnc_sum, name)
        return(args)


    def pars_lima_count_file(self, primer_info):
        """
         parsing lima_output_count
        """
        try:
          lima_count_list = []
          with open(primer_info, "r") as f:
              line = f.readlines()[1:]
              for itm in line:
                  lima_count_list.append(itm.split()[2]+itm.split()[3])
                  lima_count_list.append(itm.split()[0]+'--'+itm.split()[1])
                  lima_count_list.append(itm.split()[5])
          return(lima_count_list)
        except ValueError:
           print("Oops! Unexpected error in pars_lima_count_file.  Try again...",sys.exc_info()[0]) 

 
    def collect_primer_reads(self, primers, primers_header):
         """
           parsing lima_output.Clontech_5p--NEB_Clontech_3p.bam
         """
         try:
           faHandler = open(primers_header, 'w')
           save=pysam.set_verbosity(0)
           samfile = pysam.AlignmentFile(primers, "rb", check_sq=False)
           pysam.set_verbosity(save)
           faHandler.write("Sequence" +"\n")
           for read in samfile.fetch(until_eof=True):
               faHandler.write(read.query_name.replace("/ccs","") +"\n")
           faHandler.close()	 
         except ValueError:
           print("Oops! Unexpected error in collect_primer_reads .  Try again...",sys.exc_info()[0]) 


    def generate_plots(self, all_reads, primers_header, flnc_len, name):

      try:
           df_all_reads=pd.read_csv(all_reads, delimiter='\t', usecols = ['ZMW','ReadLengths','ScoreCombined'], low_memory = False)
           df_primers=pd.read_csv(primers_header, low_memory = False)
           common=pd.merge(df_primers, df_all_reads, left_on=['Sequence'], right_on=['ZMW'], how='outer', indicator='indicator')
           common.drop(columns=['ZMW'], inplace=True)
           common.replace('both','Clontech_5p--NEB_Clontech_3p', inplace=True)
           common.replace('right_only','No Primer', inplace=True)
           common.to_csv (name +'_binned_reads.csv', index = False, header=True)

           
           sns.set(style="darkgrid")
           bins = [1000,2000,3000, 4000, 5000,6000,7000,8000,9000,10000,11000,12000,13000,14000]
           #labels = [f'{a0}-{a1 - 1}_K' for a0, a1 in zip(bins[:-2], bins[1:-1])] + [f'≥{bins[-2]}']
           labels = [f'{a0/1000}K' for a0 in bins[:-2]] + [f'≥{bins[-2]}']
           common['type_len'] = pd.cut(common.ReadLengths, bins=bins, labels=labels, right=False)
           sns.heatmap(pd.crosstab(common.type_len, common.indicator), annot=False, fmt = '',cmap="RdYlBu_r", linewidths=0.0)
           plt.yticks(rotation=0)
           plt.title('Read Length Distribution By Primer')
           plt.tight_layout()
           plt.savefig(name + '_binned_readlengths.png',format='png')
           plt.close()

           bins = [10,20,30,40,50,60,70,80,90,100,110]
           score_labels = [f'{a0}' for a0 in bins[:-2]] + [f'≥{bins[-2]}']
           common['type_score'] = pd.cut(common.ScoreCombined, bins=bins, labels=score_labels, right=False)
           sns.heatmap(pd.crosstab(common.type_score, common.indicator), annot=False, fmt = '',cmap="RdYlBu_r", linewidths=0.0)
           plt.yticks(rotation=0)
           plt.title('Primer Quality Distribution By Primer')
           plt.tight_layout()
           plt.savefig(name + '_binned_readscores.png')
           plt.close()

           n, bins, patches = plt.hist(flnc_len, 100,  density=False, facecolor='b', alpha=0.75)
           plt.xlabel("Read Length (bp)", fontsize=10)
           plt.ylabel("Reads",fontsize=10)
           #plt.xscale('log')
           #plt.ylim([1000,40000])
           plt.grid(False)
           #plt.xlim([500,15000])
           plt.title('Read Length of Full-length Non-Concatemer Reads')
           plt.tight_layout()
           plt.savefig(name + '_flnc_readlengths.png')
           plt.close()

      except ValueError:
           print("Oops! Unexpected error in generate_plots.  Try again...",sys.exc_info()[0]) 


    def pars_flnc_report(self, flnc_rep):

        try: 
          df=pd.read_csv(flnc_rep, delimiter=",")
          unique_primers = len(df.groupby(['primer']))
          mean_df = int(df['insertlen'].mean())
          flnc_len = df['insertlen'].tolist()
          return(unique_primers, mean_df, flnc_len)

        except ValueError:
           print("Oops! Unexpected error in pars_flnc_reort.  Try again...",sys.exc_info()[0])

    def pars_flnc_summary(self, flnc_sum):
        """
          parsing flnc_filter_summary.json & generate reports
        """
 
       # flnc_rep_list=[]
       # try:
       #    with open(flnc_sum) as json_file:
       #       data = json.load(json_file)
       #       flnc_rep_list.append(data['num_reads_fl'])
       #       flnc_rep_list.append(data['num_reads_flnc'])
       #       flnc_rep_list.append(data['num_reads_flnc_polya'])

       #    return(flnc_rep_list)          
       # except ValueError:
       #    print("Oops! Unexpected error in pars_flnc_summary.  Try again...",sys.exc_info()[0])


        flnc_rep_list=[]
        try:
           with open(flnc_sum) as json_file:
              data = json.load(json_file)
              for i, item in enumerate(data['attributes']):
                   if 'num_reads_fl' == item.get("id"):
                       flnc_rep_list.append(item.get("value"))
                   if 'num_reads_flnc' == item.get("id"):
                       flnc_rep_list.append(item.get("value"))
                   if 'num_reads_flnc_polya' == item.get("id"):
                       flnc_rep_list.append(item.get("value"))
           return(flnc_rep_list)
        except ValueError:
           print("Oops! Unexpected error in pars_report .  Try again...", sys.exc_info()[0])




    def write_result(self, dict, output):
       """ 
       This file is read by MultiQC.
       """
       with open(output , 'w') as out:
          out.write('\t'.join(str(key) for key in dict)+'\n')
          out.write('\t'.join(str(value) for value in dict.values()) +'\n')


if __name__ == '__main__':
    BIR = barcode_isoseq3_reports()
    args = BIR.get_options()
    all_reads =  args[0]
    primers =    args[1]
    primer_info= args[2]
    flnc_rep =   args[3]
    flnc_sum =   args[4]
    name =       args[5]
    
    ########################
    ###### Preparing tables
    ########################

    count = len(open(all_reads).readlines(  ))
    (unique_primers, mean_df, flnc_len) = BIR.pars_flnc_report(flnc_rep)
    flnc_rep_list = BIR.pars_flnc_summary(flnc_sum)
    ccs_classification_dict_general = {
       "BioSample" : name ,
       "Reads" :  count - 1 ,
       "Reads_with_5_and_3_Primers" :  flnc_rep_list[0],
       "Non_Concatemer_Reads_with_5_and_3_Primers" : flnc_rep_list[1],
       "Non_Concatemer_Reads_with_5_and_3_Primers_and_PolyA_Tail" : flnc_rep_list[2],
       "Mean_Length_of_Full_Length_Non_Concatemer_Reads" : mean_df,
       "Unique_Primers" :  unique_primers,
       "Reads_without_Primers" :  count - flnc_rep_list[0] - 1,
       "percent_FLNC_Reads" : (flnc_rep_list[2] / (count - 1)) * 100
    }

    BIR.write_result(ccs_classification_dict_general, name + '_primer_data_general.csv')

    ## The key we want for primer_data
    wanted_keys = ['BioSample', 'Reads', 'Non_Concatemer_Reads_with_5_and_3_Primers_and_PolyA_Tail', 'percent_FLNC_Reads','Mean_Length_of_Full_Length_Non_Concatemer_Reads']
    ccs_classification_dict= dict((k, ccs_classification_dict_general[k]) for k in wanted_keys if k in ccs_classification_dict_general)
    BIR.write_result(ccs_classification_dict, name + '_primer_data.csv')   
   

    ## The key we want for primer_data_graph (Classification Details)
    wanted_keys = ['BioSample', 'Reads_with_5_and_3_Primers', 'Non_Concatemer_Reads_with_5_and_3_Primers', 'Non_Concatemer_Reads_with_5_and_3_Primers_and_PolyA_Tail','Reads_without_Primers']
    ##ccs_classification_dict_details= dict((k, ccs_classification_dict_general[k]) for k in wanted_keys if k in ccs_classification_dict_general)
    ccs_classification_dict_details = { 'BioSample': ccs_classification_dict_general['BioSample'] , \
       'Non_Concatemer_Reads_with_5_and_3_Primers_and_PolyA_Tail': ccs_classification_dict_general['Non_Concatemer_Reads_with_5_and_3_Primers_and_PolyA_Tail'], \
       'Reads_without_5_or_3_Primers': ccs_classification_dict_general['Reads_without_Primers'], \
       'Reads_only_with_5_and_3_Primers': ccs_classification_dict_general['Reads_with_5_and_3_Primers'] - ccs_classification_dict_general['Non_Concatemer_Reads_with_5_and_3_Primers'], \
       'Non_Concatemer_Reads_only_with_5_and_3_Primers': ccs_classification_dict_general["Non_Concatemer_Reads_with_5_and_3_Primers"] - ccs_classification_dict_general["Non_Concatemer_Reads_with_5_and_3_Primers_and_PolyA_Tail"] \
       }

    BIR.write_result(ccs_classification_dict_details, name + '_primer_data_details.csv')

 
     #with open(name +'_ccs_read_classification.csv' , 'w') as out:
     #    out.write("Analysis Metric" + "\t" + "Value" + "\n")
     #    for key, value in ccs_classification_dict.items():
     #        out.write("%s\t%d\n" %(key,value))

    lima_count_list=BIR.pars_lima_count_file(primer_info)
    primer_data_dict = dict(
    Primer_Name = lima_count_list[0],
    Primer_Index= lima_count_list[1],
    Mean_Primer_Quality= lima_count_list[2])
    
    ########################
    ###### Preparing plots
    ########################

    primers_header = os.path.splitext(os.path.basename(primers))[0]+ '.csv'
    BIR.collect_primer_reads(primers,primers_header)
    BIR.generate_plots(all_reads, primers_header, flnc_len, name)

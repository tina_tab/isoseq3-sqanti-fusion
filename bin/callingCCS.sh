#!/bin/bash
#PBS -l nodes=1:ppn=12,mem=20gb,walltime=1000:00:00
#PBS -M tina.alaeitabar@gmail.com
#PBS -m ae
#PBS -j eo
#PBS -N ccs_test
#PBS -q batch
#PBS -V
#PBS -t 1


export PATH=/bioinfo/local/build/Centos/miniconda/miniconda3-4.7.12/bin:$PATH
source activate ~/work/envs_conda/test26_isoseq3
cd /data/kdi_prod/project_result/789/07.00/R065/ccs_test



#pbindex movie.subreads.bam

#MOVIE="/data/users/talaeita/git/tina_pacbio-smrtlink/pbIsoSeq3_merge/test/data/m54063_191122_223325_0.1/m54063_191122_223325.subreads.bam"
MOVIE="/data/kdi_prod/dataset_all/2011192/archive/m54063_200516_194034/m54063_200516_194034.subreads.bam"

for i in `seq 1 10`;
do
    ccs ${MOVIE} --chunk ${i}/10 -j 12 movie.ccs.${i}.consensusreadset.xml --report-file movie.ccs.${1}.report.txt --max-length 15000 --min-length 50 --min-passes 1 --min-rq 0.8 --min-snr 2.5 --skip-polish --log-level INFO
done



#ccs ${MOVIE} --chunk 1/3 -j 12 movie.ccs.1.consensusreadset.xml  --report-file movie.ccs.1.report.txt --max-length 15000 --min-length 50 --min-passes 1 --min-rq 0.8 --min-snr 2.5 --skip-polish --log-level INFO


#ccs ${MOVIE} --chunk 2/3 -j 12 movie.ccs.2.consensusreadset.xml --report-file movie.ccs.2.report.txt --max-length 15000 --min-length 50 --min-passes 1 --min-rq 0.8 --min-snr 2.5 --skip-polish --log-level INFO


#ccs ${MOVIE} --chunk 3/3 -j 12 movie.ccs.3.consensusreadset.xml --report-file movie.ccs.3.report.txt --max-length 15000 --min-length 50 --min-passes 1 --min-rq 0.8 --min-snr 2.5 --skip-polish --log-level INFO




pbmerge -o movie.ccs.bam movie.ccs.*.bam
#pbindex movie.ccs.bam 



#ccs ${MOVIE} ccs.bam -j 8

~                       

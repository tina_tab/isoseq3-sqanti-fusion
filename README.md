# PacBioIsoSeq3

**Nextflow PacBioIsoSeq3 analysis pipeline**

[![Nextflow](https://img.shields.io/badge/nextflow-%E2%89%A50.32.0-brightgreen.svg)](https://www.nextflow.io/)
[![MultiQC](https://img.shields.io/badge/MultiQC-1.9-blue.svg)](https://multiqc.info/)
[![Install with](https://anaconda.org/anaconda/conda-build/badges/installer/conda.svg)](https://conda.anaconda.org/anaconda)
[![Singularity Container available](https://img.shields.io/badge/singularity-available-7E4C74.svg)](https://singularity.lbl.gov/)
[![Docker Container available](https://img.shields.io/badge/docker-available-003399.svg)](https://www.docker.com/)

### Introduction

This pipeline provides a detailed characteristics of the full complement of isoforms across an entire transcriptome.
It was designed based on [IsoSeq clustering](https://github.com/PacificBiosciences/IsoSeq/blob/master/isoseq-clustering.md) methode and contains the newest tools to identify transcripts from PacBio single-molecule long reads sequencing data.

The pipeline is built using [Nextflow](https://www.nextflow.io), a workflow tool to run tasks across multiple compute infrastructures in a very portable manner. 
It comes with docker / singularity containers making installation trivial and results highly reproducible.

### Short comparison of Pipeline

<p style="text-align: center;">
<img src="docs/images/isoseq3_sequel2e.png" style="width: 80%;">
</p>
`

### Pipline summary
The option for generate automatically HiFi Reads is one of the main advantages of Sequel IIe System. 
Sequel IIe System on_instrument CCS (OICCS) outputs a reads.bam file containing one read per productive ZMW. 


<details> <summary> If HiFi reads are generated on instrument
 </summary> 

1. Extracthifi is used to extract PacBio HiFi reads (>= Q20) from full CCS output (reads.bam). ([`extracthifi`](https://github.com/PacificBiosciences/extracthifi) 

2. Refine CCS reads into full-length non-concatemer reads (FLNC). ([`lima`](https://lima.how/))/([`Trimming`](https://github.com/jeffdaily/parasail))/([`identification`](https://github.com/jeffdaily/parasail))

3. Perform a single clustering technique to identify unique isoforms from FLNC ([`isoseq-clustering`](https://github.com/PacificBiosciences/IsoSeq/blob/master/isoseq-clustering.md))

4. Align isoforms on reference genome and refine theirs redundant ([`Minimap2`](https://github.com/lh3/minimap2))/([`Cupcake`](https://github.com/Magdoll/cDNA_Cupcake/wiki/Cupcake:-supporting-scripts-for-Iso-Seq-after-clustering-step))

5. Evaluate the quality and transcript structural ([`SQANTI3`](https://github.com/ConesaLab/SQANTI3))
  
</details>

<details> <summary> If HiFi Reads are not generated automatically </summary> 

1. Generate circular consensus sequence reads from the sub-reads generated from the sequencing run. ([`CCS`](https://github.com/PacificBiosciences/ccs)) 

2. Refine CCS reads into full-length non-concatemer reads (FLNC). ([`lima`](https://lima.how/))/([`Trimming`](https://github.com/jeffdaily/parasail))/([`identification`](https://github.com/jeffdaily/parasail))

3. Perform a single clustering technique to identify unique isoforms from FLNC ([`isoseq-clustering`](https://github.com/PacificBiosciences/IsoSeq/blob/master/isoseq-clustering.md))

4. Align isoforms on reference genome and refine theirs redundant ([`Minimap2`](https://github.com/lh3/minimap2))/([`Cupcake`](https://github.com/Magdoll/cDNA_Cupcake/wiki/Cupcake:-supporting-scripts-for-Iso-Seq-after-clustering-step))

5. Evaluate the quality and transcript structural ([`SQANTI3`](https://github.com/ConesaLab/SQANTI3))

</details>


### Quick help

```bash

>> N E X T F L O W  ~  version 20.10.0
pacbioisoseq3 v1.0.0
=========================================================
Usage:
nextflow run main.nf --samplePlan sample_plan -profile conda
nextflow run main.nf --samplePlan 'sample_plan.csv' -profile conda --genomeAnnotationPath '/data/annotations/pipelines' --genome 'hg19'
=========================================================
    Mandatory arguments:
        --samplePlan 'SAMPLEPLAN'       Path to sample plan input file (cannot be used with --reads)
        --genome [str]                  Name of genome reference. See the '--genomeAnnotationPath' to defined the annotations path
        --genomeAnnotationPath [dir]    Path to genome annotation folder
        -profile PROFILE                Configuration profile to use test/conda/singularity/cluster (see below)
====================================
    Circular Consensus Sequence (CCS):
        --nbChunks [int]                Parallelize CCS via chunk.
====================================
    Primer removal:
        --barcode  'BARCODESET'         Path to fasta or barcodeset file
====================================
    Other options:
        --outDir 'PATH'                 The output directory where the results will be saved
        --metadata 'FILE'               Add metadata file for multiQC report
        -name 'NAME'                    Name for the pipeline run. If not specified, Nextflow will automatically generate a random mnemonic
====================================                                 
    Skip options:
        --skip_OICCS                  Disable automatic HiFi reads generation with Sequel IIe
        --skip_mapping                Disable isoforms mapping
        --skip_sqanti                 Disable sqanti step
        --skip_fusion                 Disable find fusion
        --skip_multiqc                Disable MultiQC step

====================================
    Available Profiles
        -profile test                   Set up the test dataset
        -profile conda                  Build a new conda environment before running the pipeline
        -profile multiconda             Build a new conda environment per process before running the pipeline.
                                        Use `--condaCacheDir` to define the conda cache path.
                                   
        -profile condaPath              Use a pre-build conda environment already installed on our cluster 
        -profile singularity            Use the Singularity images for each process
        -profile cluster                Run the workflow on the cluster, instead of locally
```

### Quick run

The pipeline can be run on any infrastructure from a list of input files or from a sample plan as follow

#### Run the pipeline on the test dataset
See the conf/test.conf to set your test dataset.

```
nextflow run main.nf -profile test,conda
```


#### Run the pipeline from a sample plan

```
nextflow run main.nf --samplePlan MY_SAMPLE_PLAN --genome 'hg19' --outDir MY_OUTPUT_DIR -profile conda
```

#### Run the pipeline on a cluster

```
echo "nextflow run main.nf --samplePlan MY_SAMPLE_PLAN --genome 'hg19' --outDir MY_OUTPUT_DIR -profile singularity,cluster"  | qsub -N IsoSeq
```
### Defining the '-profile'
By default (whithout any profile), Nextflow will excute the pipeline locally, expecting that all tools are available from your PATH variable.

In addition, we set up a few profiles that should allow you i/ to use containers instead of local installation, ii/ to run the pipeline on a cluster instead of on a local architecture.

The description of each profile is available on the help message (see above).

Here are a few examples of how to set the profile option. See the full documentation for details.

#######  Run the pipeline locally, using the paths defined in the configuration for each tool (see conf/path.config)
```
-profile path --globalPath INSTALLATION_PATH 
```

#######  Run the pipeline on the cluster, using the Singularity containers
```
-profile cluster,singularity --singularityPath SINGULARITY_PATH 
```

####### Run the pipeline on the cluster, building a new conda environment
```
-profile cluster,conda --condaCacheDir CONDA_CACHE
```


### Sample Plan

A sample plan is a csv file (comma separated) that list all samples with their biological names, **with no header**.

```
[BIOLOGICAL_NAME | PATH_TO_SMRT-cell_SUBREADS_NAME (without suffix)]
```

### Full Documentation

1. [Installation](docs/installation.md)
2. [Reference genomes](docs/referenceGenomes.md)
3. [Running the pipeline](docs/usage.md)
4. [Output and how to interpret the results](docs/output.md)


For any question, bug or suggestion, please, contact Tina.Alaeitabar.



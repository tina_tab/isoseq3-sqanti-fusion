# Output

This document describes the output produced by the pipeline. This pipeline generates a final report to summarise results at the end of the pipeline by Multiqc [`Multiqc`](https://multiqc.info/).


## Pipeline overview
The pipeline is built using [`Nextflow`](https://www.nextflow.io/) for producing Single Molecule, Real-Time (SMRT) Sequencing data to transcriptome analysis.


Briefly, the workflow is based on [`IsoSeq clustering`](https://github.com/PacificBiosciences/IsoSeq/blob/master/isoseq-clustering.md) methode and runs several tools of [`PacBio`](https://www.pacb.com/applications/rna-sequencing/) in order to provide a wide range of descriptors of transcript quality and generate a graphical report. This pipeline is also able to make the suitable format files for launching [`Functional IsoTranscriptomics Analyses`](https://tappas.org/).

The directories listed below will be created in the output directory after the pipeline has finished.

# **CCS and mergingCCS**
<hr style="border:1px solid #b2b2b2"> </hr>
The option for generate automatically HiFi Reads is one of the main advantages of Sequel IIe System. 
Sequel IIe System on_instrument CCS (OICCS) outputs a reads.bam file containing one read per productive ZMW. 

- If HiFi reads are generated on instrument, then  extracthifi is used to extract PacBio HiFi reads (>= Q20) from full CCS output (reads.bam). 

<details> <summary> Output files </summary> 
**Output directory: `results/CCS/`**

  * [SAMPLE].ccs.bam and  [SAMPLE].ccs.bai
  
</details>

- If HiFi Reads are not generated automatically `(skip_OICCS = true)`, then [`CCS`](https://github.com/PacificBiosciences/ccs) generates circular consensus sequence reads from the sub-reads generated from the sequencing run. This step generate HiFi reads from the raw data with a minimum of 3 passes, 50 as minimum length and quality > Q20.
     - You can parallelize CCS by chunking via `--chunk`. After use this option the pipeline will merge chunks automatically.

<details> <summary> Output files </summary> 
**Output directory: `results/CCS/`**

  * [SAMPLE].[chunk_number].ccs.bam and [SAMPLE].mapped.bam.pbi
    * The CCS bam file for each SMRT cell per chunk and PacBio index file 
  
**Output directory: `results/mergingCCS/`**

  * [SAMPLE].ccs.bam and [SAMPLE].ccs.bam.pbi
    * The merged CCS bam file for each SMRT cell and PacBio index file
</details>

# **CCS Report**
<hr style="border:1px solid #b2b2b2"> </hr>
For each merged CCS files, the current workflow generates summary statistics on CCS number, length and number of passes for each SMRT cell. The results are available in MultiQC through tables and graphical representation.

<details> <summary> Output files </summary> 

**Output directory: `results/ccsReport/`**

  * [SAMPLE]_ccs_info.csv
  * [SAMPLE]_ccs_readlengths.png
  * [SAMPLE]_ccs_summary_metrics.csv
  * [SAMPLE]_number_of_passes.png
</details>  

<p style="text-align: center;">
<img src="images/ccsreport/CCSout.png" style="width: 90%;">
</p>

# **Lima**
<hr style="border:1px solid #b2b2b2"> </hr>
The process of demultiplexing CCS read to generate full-length reads is done with [`lima`](https://lima.how/). A full-length transcript contains a proper cDNA 5' primer and 3' cDNA primer which also allows to identify its orientation. 

The output files be named according to their primer pair.

<details> <summary> Output files </summary> 

**Output directory: `results/lima/`**

  * [SAMPLE].fl.datastore.[primer-pair].bam
  * [SAMPLE].fl.datastore.[primer-pair].bam.pbi
  * [SAMPLE].fl.datastore.[primer-pair].subreadset.xml
  * [SAMPLE].fl.datastore.json and [SAMPLE].fl.datastore.lima.clips
  * [SAMPLE].fl.datastore.lima.counts
  * [SAMPLE].fl.datastore.lima.guess
  * [SAMPLE].fl.datastore.lima.report
  * [SAMPLE].fl.datastore.lima.summary
  
  > **NB**: If you need to know in detail about the output files, please see [`here`](https://lima.how/output/).
</details>  


# **Refine**
<hr style="border:1px solid #b2b2b2"> </hr>
The last stap in the pipeline generates only full-length reads which still needs to be refined from poly(A) tails by 


   * [`Trimming`](https://github.com/jeffdaily/parasail) of poly(A) tails
   * Rapid concatemer [`identification`](https://github.com/jeffdaily/parasail) and removal

This step generates full-length non-concatemer reads. A FLNC contains the 5' cDNA primer, 3' cDNA primer and polyA tail sequences, and if doesn''t contain additional cDNA primers in the middle of the sequence (suggesting an artificial chimerical cDNA). See [`here`](https://github.com/PacificBiosciences/IsoSeq/blob/master/isoseq-clustering.md) for more information.

<details> <summary> Output files </summary> 

**Output directory: `results/refine/`**

  * [SAMPLE].flnc.bam 
  * [SAMPLE].flnc.bam.pbi
  * [SAMPLE].flnc.consensusreadset.xml
  * [SAMPLE].flnc.filter_summary.json
  * [SAMPLE].flnc.report.csv
  
</details> 
  
    
<p style="text-align: center;">
<img src="images/barcodeIsoseqReport/TA_3784_0881_flnc_readlengths.png" style="width: 50%;">
</p>

# **Barcodes IsoSeq3 Reports**
<hr style="border:1px solid #b2b2b2"> </hr>
This script shows read length and the quality of primers at various levels of data and summarised in the MultiQC report.

<details> <summary> Output files </summary> 
**Output directory: `results/barcodeIsoseqReport/`**

  * [SAMPLE].fl.datastore.Clontech_5p--NEB_Clontech_3p.csv
  * [SAMPLE]_binned_reads.csv
  * [SAMPLE]_primer_data.csv
  * [SAMPLE]_primer_data_details.csv
  * [SAMPLE]_primer_data_general.csv
  
</details> 

</br>

<p style="text-align: center;">
<img src="images/barcodeIsoseqReport/out.png" style="width: 90%;">
</p>

<p style="text-align: center;">
<img src="images/CCS Read_Classification_Details/barplot_primer_data_details.png" style="width: 80%;">
</p>

# **Clustering**
<hr style="border:1px solid #b2b2b2"> </hr>
When the FLNC are available, the pipeline clusters sequencing reads to identify unique isoforms based on a hierarchical alignment strategy with O(N*log(N)). A polish step is then applied to separate these isoforms depending on their quality into 2 fasta files. High Quality (HQ) for isoforms with accuracy ≥ 99% and Low Quality (LQ) for isoforms with accuracy < 99%. 

<details> <summary> Output files </summary> 
**Output directory: `results/clustering/`**

  * [SAMPLE].clustered.bam
  * [SAMPLE].clustered.bam.pbi
  * [SAMPLE].clustered.hq.bam
  * [SAMPLE].clustered.hq.bam.pbi
  * [SAMPLE].clustered.lq.bam
  * [SAMPLE].clustered.lq.bam.pbi
  * [SAMPLE].clustered.transcriptset.xml
</details> 

# **Mapping**
<hr style="border:1px solid #b2b2b2"> </hr>
In this step the pipeline aligns the sequencing reads on the reference by using [`Minimap2`](https://github.com/lh3/minimap2). The aligned data are then used to generate a final annotation file in the next step. 

<details> <summary> Output files </summary> 
**Output directory: `results/mapping/`**

  * [SAMPLE].mapped.bam
  * [SAMPLE].mapped.bam.pbi
  
</details> 

<p style="text-align: center;">
<img src="images/clustering_mapping_isocolape/TA_3766_0881_Read_Length_of_Mapped_Isoforms.png" style="width: 50%;">
</p>

# **Collapse**
<hr style="border:1px solid #b2b2b2"> </hr>
When the mapped information are available the pipeline induces a depth reduction to remove redundant isoforms. The current version includes [`Cupcake`](https://github.com/Magdoll/cDNA_Cupcake/wiki/Cupcake:-supporting-scripts-for-Iso-Seq-after-clustering-step) which is a set of downstream scripts for processing Iso-Seq data.

<details> <summary> Output files </summary> 
**Output directory: `results/collapse/`**

  * [SAMPLE].collapse.gff
  * [SAMPLE].collapse.report.json
  * [SAMPLE]_isocollapse_report.csv
  * [SAMPLE]_Read_Length_of_Mapped_Isoforms.png
  * [SAMPLE]_transcript_mapping_summary_metrics.csv

</details> 

# **Clustering Mapping Collapse**
<hr style="border:1px solid #b2b2b2"> </hr>
This script gives numerous statistics about the isoforms classification and covered loci upon mapping result.

<details> <summary> Output files </summary> 
**Output directory: `results/clustering_mapping_isocolaps/`**

  * [SAMPLE]_clustring_mapping_isocollapse_metrics.csv
  * [SAMPLE]_Read_Length_of_Mapped_Isoforms.png

</details> 

# **SQANTI**
<hr style="border:1px solid #b2b2b2"> </hr>
[`SQANTI`](https://github.com/ConesaLab/SQANTI3) is a package of scripts designed to evaluate the quality and transcript structural annotation. It presents the characterization of isoforms upon genes, transcripts and splice junctions annotation compared to the reference catalog. You can of course also provide SQANTI files directly as input to [`tappAS`](https://tappas.org/) in order to analyse the functional impact of differential splicing.

<details> <summary> Output files </summary> 
**Output directory: `results/sqanti/`**

  * [SAMPLE].collapse_classification.txt
  * [SAMPLE].collapse_corrected.gtf
  * [SAMPLE].collapse_junctions.txt
  * [SAMPLE].collapse_sqanti_report.pdf
</details> 

# **SQANTI Report**
<hr style="border:1px solid #b2b2b2"> </hr>
This pipeline extracts the main annotations results from SQANTI in 3 sections which are available in MultiQC through tables and graphical representation.

  * This first section presents the genes annotations. Each identified gene is compared to the reference catalog to be classified in 2 categories: <span style="color:green">Annotated</span> or <span style="color:green">Novel Gene</span>.

  * The second section reports the isoforms classification based on the comparison between their splice junctions and the reference transcriptome provided. 9 following categories are defined in SQANTI:

    * <span style="color:green">Full Splice Match (FSM)</span>: meaning the reference and query isoform have the same  number of exons and each internal junction agree. The exact 5'' start and 3'' end can differ by any amount. 
 
    * <span style="color:green">Incomplete Splice Match (ISM)</span>: the query isoform has fewer 5'' exons than the reference, but each internal junction agree. The exact 5'' start and 3'' end can differ by any amount.

    * <span style="color:green">Novel In Catalog (NIC)</span>: the query isoform does not have a FSM or ISM match, but is using a combination of known donor/acceptor sites.

    * <span style="color:green">Novel Not in Catalog (NNC)</span>: the query isoform does not have a FSM or ISM match, and has at least one donor or acceptor site that is not annotated. 

    * <span style="color:green">Genic intron</span>: the query isoform is completely contained within an annotated intron. 

    * <span style="color:green">genic_genomic</span>: the query isoform overlaps with introns and exons. 

    * <span style="color:green">Intergenic</span>: the query isoform is in the intergenic region.

    * <span style="color:green">Antisens</span>: the query isoform overlaps the complementary strand of an annotated transcript.

    * <span style="color:green">Fusion</span>: the query isoform overlaps with two annotated different loci.

 * The third section shows splice junctions annotation based on their type ( <span style="color:green">canonical</span> or  <span style="color:green">non-canonical</span>) and their presence in the reference (<span style="color:green">known</span> or <span style="color:green">novel</span>). The canonical status refers to the nature of the two pairs of dinucleotides present at the beginning and the end of the introns encompassed by the junction. GT-AG as well as GC-AG and AT-AC are considered as canonical splicing (as they are found in > 99,9% of all human introns). And the other possible combination are considered as non-canonical splicing.

<details> <summary> Output files </summary> 

**Output directory: `results/sqantiReport/`**

  * [SAMPLE].category_genes.csv
  * [SAMPLE].category_isoforms.csv
  * [SAMPLE].category_junctions.csv

</details> 

<p style="text-align: center;">
<img src="images/sqanti/isoform_annotation.png" style="width: 80%;">
</p>

<br/>
<p style="text-align: center;">
<img src="images/sqanti/splice_junction_classification.png" style="width: 80%;">
</p>

<br/>

# **Fusion Finder**
<hr style="border:1px solid #b2b2b2"> </hr>
[`Cupcake`](https://github.com/Magdoll/cDNA_Cupcake) does a major overhaul of finding fusion genes.
This tool is able to detect the gusion transcripts. Then the pipeline uses again [`SQANTI`](https://github.com/ConesaLab/SQANTI3) which now supports classifying fusion transcripts. Now that we have SQANTI3 classification we can filter for fusion candidates based on:

    Both genes must be from different genes
    Both genes must be annotated (based on SQANTI3 classification)

And this will generate an output table that shows breakpoints, exon counts and etc.


# **MultiQC**
<hr style="border:1px solid #b2b2b2"> </hr>
[`MultiQC`](https://multiqc.info/) is a visualisation tool that generates a single HTML report summarising all SMRT Cell in your project. Most of the pipeline QC results are visualised in the report and further statistics are available in within the report data directory.

<details> <summary> Output files </summary> 

**Output directory: `results/MultiQC/`**

  * PacBioRunQC_report.html
    * MultiQC report - a standalone HTML file that can be viewed in your web browser
  * multiqc_data/
    * Directory containing parsed statistics from the different tools used in the pipeline

</details> 

> **NB:** The MultiQC plots displayed Only if skip_multiqc has not been specified.


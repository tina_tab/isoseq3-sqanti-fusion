# Isoseq3: Usage

## Table of contents

* [General Nextflow info](#general-nextflow-info)
* [Running the pipeline](#running-the-pipeline)
* [Main arguments](#main-arguments)
    * [`samplePlan`](#samplePlan)
* [Reference genomes](#reference-genomes)
    * [`--genome`](#--genome)
    * [`--genomeAnnotationPath`](#--genomeAnnotationPath)
* [Profiles](#profiles)
* [Job resources](#job-resources)
* [Automatic resubmission](#automatic-resubmission)
* [Custom resource requests](#custom-resource-requests)
* [Other command line parameters](#other-command-line-parameters)
    * [`--nbChunks`](#--nbChunks)
    * [`--barcode`](#--barcode)
    * [`--skip*`](#--skip*)
    * [`outdir`](#outdir)
    * [`name`](#name)
    * [`resume`](#resume)
    * [`c`](#c)
    * [`max_memory`](#max_memory)
    * [`max_time`](#max_time)
    * [`max_cpus`](#max_cpus)

## General Nextflow info
Nextflow handles job submissions on SLURM or other environments, and supervises running the jobs. Thus the Nextflow process must run until the pipeline is finished. We recommend that you put the process running in the background through `screen` / `tmux` or similar tool. Alternatively you can run nextflow within a cluster job submitted your job scheduler.

It is recommended to limit the Nextflow Java virtual machines memory. We recommend adding the following line to your environment (typically in `~/.bashrc` or `~./bash_profile`):

```bash
NXF_OPTS='-Xms1g -Xmx4g'
```
## Running the pipeline
The typical command for running the pipeline is as follows:

```bash
nextflow run main.nf -profile test
```


Note that the pipeline will create the following files in your working directory:

```bash
work            # Directory containing the nextflow working files
results         # Finished results (configurable, see below)
.nextflow_log   # Log file from Nextflow
# Other nextflow hidden files, eg. history of pipeline runs and old logs.
```

## Main arguments

### `--samplePlan`
Use this to specify the location of your input data, which contains one or more BAM files containing the raw unaligned subreads. For example:

```bash
--dataSet 'path/to/dataset'
```

## Reference genomes

The pipeline config files come bundled with paths to the genomes reference files. 

### `--genome`

There are different species supported in the genomes references file. To run the pipeline, you must specify which to use with the `--genome` flag.

You can find the keys to specify the genomes in the [genomes config file](../conf/genomes.config). Common genomes that are supported are:

* Human
  * `--genome hg38`
  * `--genome hg19`

> There are numerous others - check the config file for more.

Note that you can use the same configuration setup to save sets of reference files for your own use, even if they are not part of the genomes resource. 
See the [Nextflow documentation](https://www.nextflow.io/docs/latest/config.html) for instructions on where to save such a file.

The syntax for this reference configuration is as follows:

```nextflow
params {
  genomes {
    'hg19' {
      fasta            = '<path to genome fasta file for identito monitoring>'
      gtf              = '<path to GTF annotation file>'
    }
    // Any number of additional genomes, key is used with --genome
  }
}
```

Note that these paths can be used on the following steps:
- `mapping`
- `sqanti`
- `fusion finder`

### `-profile`
Use this parameter to choose a configuration profile. Profiles can give configuration presets for different compute environments. Note that multiple profiles can be loaded, for example: `-profile singularity` - the order of arguments is important!

If `-profile` is not specified at all the pipeline will be run locally and expects all software to be installed and available on the `PATH`.

* `test`
    * A profile with a complete configuration for automated testing
    * Includes links to test data so needs no other parameters

Look at the [Profiles documentation](profiles.md) for details.


## Job resources

### Automatic resubmission

Each step in the pipeline has a default set of requirements for number of CPUs, memory and time (see the `conf/base.conf` file). 
For most of the steps in the pipeline, if the job exits with an error code of `143` (exceeded requested resources) it will automatically resubmit with higher requests (2 x original, then 3 x original). If it still fails after three times then the pipeline is stopped.

### Custom resource requests

Wherever process-specific requirements are set in the pipeline, the default value can be changed by creating a custom config file. See the files hosted at [`pacbioisoseq3/configs`](https://github.com/git/pacbioisoseq3/conf) for examples.

Please make sure to also set the `-w/--work-dir` and `--outdir` parameters to a S3 storage bucket of your choice - you'll get an error message notifying you if you didn't.


## Other command line parameters

### `--nbChunks`

If the option for generate automatically consensus reads is off during sequencing, the pipeline is capable to generated Hifi reads from subreads by chunking and in parallel.  


### `--barcode`

This file is used for demultiplexing sequences in PacBio single-molecule sequencing data.


### `--skip*`

The pipeline contains diffrent steps. Sometimes, it may not be desirable to run all of them if time and compute resources are limited.
The following options make this easy:


* `--skip_OICCS` -                  Disable automatic HiFi reads generation with Sequel IIe
* `--skip_mapping` -                Disable isoforms mapping
* `--skip_sqanti` -                 Disable sqanti step
* `--skip_fusion` -                 Disable find fusion
* `--skip_multiqc` -                Disable MultiQC step


### `--outdir`

The output directory where the results will be saved.

### `--name`

Name for the pipeline run. If not specified, Nextflow will automatically generate a random mnemonic.

This is used in the MultiQC report (if not default).

**NB:** Single hyphen (core Nextflow option)

### `-resume`

Specify this when restarting a pipeline. Nextflow will used cached results from any pipeline steps where the inputs are the same, continuing from where it got to previously.

You can also supply a run name to resume a specific run: `-resume [run-name]`. Use the `nextflow log` command to show previous run names.

**NB:** Single hyphen (core Nextflow option)

### `-c`

Specify the path to a specific config file (this is a core NextFlow command).

Note - you can use this to override pipeline defaults.

### `--max_memory`

Use to set a top-limit for the default memory requirement for each process.
Should be a string in the format integer-unit. eg. `--max_memory '8.GB'`

### `--max_time`

Use to set a top-limit for the default time requirement for each process.
Should be a string in the format integer-unit. eg. `--max_time '2.h'`

### `--max_cpus`

Use to set a top-limit for the default CPU requirement for each process.
Should be a string in the format integer-unit. eg. `--max_cpus 1`

